*********************
Waypointing
*********************

.. Note::  Parts of this Text were directly taken and tweaked from PODBot 2.5 Readme File

Brief information
==========================

Waypoints, what are they?
--------------------------

Waypoints are locations in the maps that the bots will use when navigating. They allow the bot to roam about the map without having to blindly bump into walls. Waypoints can be thought of like the bread crumbs in the Hansel and Gretel story. They are something marking a path to make it easier to follow. The waypoints are not displayed while the game is being played. The bots "know" which way to go by following the path laid out by these waypoints.

In order to get the bots to cover the entire map when playing the game, waypoints should be laid out all over the map. Without the waypoints the bots will just randomly run around and will tend to stay in the same area. Waypoints also allow the bot to know where specific goals are in the game. You couldn't play capture the flag unless you knew where the flag was and where the flag needed to be taken after being captured. Waypoints are what makes all of these things possible. Bots with sufficiently advanced waypoint information and algorithms would be able to find the shortest distance between points in the map and navigate from their current position to some goal on the other side of the map.

This document describes the commands needed to create or edit a waypoint file. When creating or editing a waypoint file it is best NOT to have any bots wandering around at the same time. Create a LAN game, and join the game then start laying out waypoints for a map. Save the waypoint file, disconnect from the server, then start the game again, this time with the bots to see how well (or poorly) they follow the waypoints you have created.


What do waypoints look like in the map?
--------------------------

The waypoints are displayed using vertical lines that look like lightning or electric streams. The vertical line used to draw the waypoint is the same height as the player. The point in space at the center of this vertical line is what is actually used as the waypoint. Different Types of waypoints have different colors. 

Waypoints are stored in a file with the same name as the map file name but using the extension ``.pwf`` instead of the map file extension of ``.bsp``. For example, the waypoint file for the map de_dust would be de_dust.pwf. It will be stored in the ``Half-Life\cstrike\addons\yapb\data`` folder and then in the folder you specified with ``yb_wptsubfolder``.
Waypoint files are automatically loaded when the map is loaded (if they exist). If the waypoint file does not exist, you will need to create it using the directions contained in this document.

Waypoint console commands Summary
--------------------------

The following Waypoint commands are available (note these ARE case sensitive):
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb wp on``              | Turns on displaying of waypoints.                                                                                              |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb wp on noclip``       | Turns on waypoint editing with noclip cheat. This allows you to fly and you don't collide with walls.                          |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb wp add``             | Adds a waypoint at the current player location. A Menu will pop up where you have to select the different types of waypoints.  |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb wp addbasic``        | Adds basic waypoints on map, like spawn points, goals and ladders.                                                             |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb wp delete``          | Deletes the waypoint nearest to the player (see below).                                                                        |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb wp flags``           | Allows you to manually add/remove Flags to a waypoint.                                                                         |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb wp setradius x``     | Manually sets the Wayzone Radius for this waypoint to value x.                                                                 |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb wp teleport x``      | Teleport hostile to specified in value x waypoint.                                                                             |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb wp find x``          | Show direction to specified in value x waypoint.                                                                               |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb wp stats``           | Shows the number of different waypoints you did already set.                                                                   |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb wp check``           | Checks if all Waypoints connections are valid.                                                                                 |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb wp load``            | Loads the waypoints from a waypoint file (see below).                                                                          |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb wp save``            | Saves the current waypoints to a file (see below).                                                                             |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb wp save nocheck``    | Saves the current waypoints to a file without validating.                                                                      |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb wp menu``            | Show the waypoint editor menu.                                                                                                 |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb autowp``             | Displays the status of the autowaypoint setting.                                                                               |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb autowp on``          | Turns on autowaypoint setting (see below).                                                                                     |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb autowp off``         | Turns off autowaypoint setting (see below).                                                                                    |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb path autodistance``  | Opens menu for setting autopath maximum distance.                                                                              |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb path cache``         | Remember the nearest to player waypoint.                                                                                       |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb path create``        | Opens menu for path creation.                                                                                                  |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb path delete``        | Delete path from cached (or faced) to nearest waypoint.                                                                        |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb path create_in``     | Creating incoming path connection from cached (faced) to nearest waypoint.                                                     |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb path create_out``    | creating outgoing path connection from cached (faced) to nearest waypoint.                                                     |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb path create_both``   | Creating both-ways path connection from cached (faced) to nearest waypoint.                                                    |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+

To use the waypoint commands, you will have to use the console. You must start the game with the console option enabled (use ``hl.exe -console`` in the Target: line of a shortcut).  Use the ``~`` key to bring down the console. Enter the console commands that you wish, then use the ``~`` key again to return to the game.

You can make things easier by binding console commands to keys on the keyboard. Just type ``bind`` followed by the key you want to bind to, followed by the console command. You will have to use double quotes on the console command if it's more than one word. Here's an example of console commands that we used when editing waypoints::

    bind a "yb wp add"
    bind d "yb wp delete"
    bind l "yb wp load"
    bind w "yb wp save"

The ``a`` key will manually add a waypoint. The ``d`` key will delete a waypoint. The ``l`` key will load the most recently saved waypoint file (useful if you have added some waypoints but don't want to save them and want to go back to the previously save waypoint list, sort of like an "undo"). The ``w`` key (for "write waypoints") will save the list of waypoints to the waypoint file.

Using `yb wp add`` will place a waypoint at the location that the player is currently at in a map. Waypoints can be placed anywhere the player can go. Waypoints can even be placed in mid-air (as long as the player can get to that location). One way to do this is by jumping or running off of a ledge or cliff. When placing a waypoint, you will hear a sound to indicate that the waypoint has been dropped (the sound is the same sound the crossbow bolt makes when striking an wall). You then have to specify the Type of waypoint you want to place here.

Waypont types
==========================

Normal Waypoint
--------------------------

.. figure:: static/img01.jpg
    :align: center

    Normal Waypoint.

Sets a Waypoint which is used for normal walking from one point to each other. Color is green.

Terrorist/CT Important Waypoint
--------------------------

.. figure:: static/img02.jpg
    :align: center

    CT Important Waypoint.

.. figure:: static/img03.jpg
    :align: center

    Terrorist Important Waypoint.

These are strategical waypoints for that team. Bots use these waypoints to patrol the map. How they use this, heavily depends on the map type. In a DE Map you should place a lot of CT Waypoints around the Bomb Plant Zone and only 2 or 3 strategical waypoints for Terrorists. On a DE Map Counter Bots will most of the time pick a Counter Waypoint as a destination and only sometimes pick a Terrorist waypoint. Terrorists are allowed to pick any of them as a goal waypoint. This behaviour changes with the maptype, in a CS map for example it is vice versa. You NEED to have some of these, but don't place too many. Average count for this is below 10 for each. Terrists WP Color is red, Counter Waypoints are blue.

Block with Hostage / Ladder Waypoint
--------------------------

.. figure:: static/img04.jpg
    :align: center

    Ladder Waypoint.

If you set this flag on a waypoint, bots won't take this waypoint if a hostage is following them. It's also used for ladders.

Rescue Waypoint
--------------------------

.. figure:: static/img05.jpg
    :align: center

    Rescue Waypoint.

This is only needed on Hostage Maps. It's the location where you want the CT Bots to rescue the hostages. Usually only 1 is needed. Color is white.

Goal Waypoint
--------------------------

.. figure:: static/img06.jpg
    :align: center

    Rescue Waypoint.

Place this at a position where Goals of the Map need to be achieved. In DE maps this is a bomb plant spot and on cs maps Counter look here to rescue some hostages. In AS maps these are the Escape points for the VIP. Color is purple.

Campstart Waypoint
--------------------------

.. figure:: static/img07.jpg
    :align: center

    Rescue Waypoint.

This is a camping (or sniper if you prefer) waypoint. To place it, look into the direction you want the Bot to start looking at when camping. To let him crouch at that position just press the duck button while adding it. You also need to specify the:

--------------------------
Campend Waypoint
Selecting this will set the end direction for the Bot to look at when camping. Look straight into the direction where you want the Bot to look and select it. Note: you can't set a campend waypoint before doing a campstart waypoint. Normal Color is cyan, if you made it team specific it will have a touch of red if Terrorist WPT or blue if CT.

Jump Waypoint
--------------------------

.. figure:: static/img08.jpg
    :align: center

    Rescue Waypoint.

By selecting it, your moves will be monitored and if you jump, a normal waypoint will be placed at the position you started to jump and another will be placed where you landed. The Waypoint Type as said before is a standard waypoint but a red connection line will show you that there is a jump connection between the two. You can also create jump connections between already existing waypoints, just make sure you're in the near of the first when jumping and land in the near of the destination waypoint. Note that this will adjust the waypoints origin on the average position of the previous position and your jump position. You can even have the bots jump to a ladder or do several jumps after each other!

.. Note::  To set a crouch waypoint, just keep holding your duck key. Also note that placing the "Important" waypoints differs from maptype to maptype. On defusion maps, Terrorist will be able to choose a Terrorist Waypoint OR a Counter Waypoint. Counter stay most of the time at Counter Waypoints. On cs maps, Terrorists choose Terrorists waypoints most of the time and Counters can walk to any Counter waypoint OR Terrorist waypoint. All other waypoints will be chosen team independent (even camp spots).

Using waypoint commands
=========================

Using ``yb wp delete`` will remove the waypoint closest to the player. The waypoint MUST be within 55 units from the player (about 1/2 the player height) in order to be removed. You will need to stand fairly close to the waypoint to be able to remove it. This prevents you from accidentally removing a waypoint on the other side of the room. When removing a waypoint you will hear a sound indicating that the waypoint was removed (the same sound the tripmine makes when placed on a wall).

Using ``yb wp save`` will save the waypoint data to the waypoint file. The waypoint file will have the same name as the current map with an extension of ``.pwf``. The file will be saved into the ``cstrike/addons/yapb/data`` Folder. Your current player name will be saved as the waypoint file author.

Using ``yb wp load`` will clear out all waypoints in the current map and load them from the waypoint file in the maps folder. This is a good way to "undo" a bunch of waypoints that you have created but do not wish to save. There is no way to ``undo`` a single waypoint. You will have to use the ``yb wp delete`` command to remove waypoints one-by-one.

The ``yb autowp`` command allows you to automatically drop waypoints as you run around in a map. To turn autowaypointing on use ``yb autowp on``. As you run around the level waypoints will be dropped every 200 units automatically. No waypoint will be dropped if another waypoint is already within 200 units of your current position. So if you want to place lots of waypoints fairly close together you may have to manually place some of the waypoints using the ``yb wp add`` command. Autowaypointing keeps track of where the last waypoint was dropped (either manually or from autowaypointing) and will place another waypoint when you are 200 units from the last waypoint. If you don't like where autowaypointing placed a waypoint and want to move it a little bit, you can delete the waypoint using ``yb wp delete`` (but turn off autowaypointing before, since it will place a new waypoint otherwise). 

When using autowaypointing, try to stay in the center of narrow hallways and always place a waypoint on BOTH sides of a door. You may have to place some of these waypoints manually using ``yb wp add`` since places like intersections of hallways and doorway entrances and exits don't usually fall exactly at the location where autowaypoint would want to place a waypoint.

Whenever you get close to a waypoint yellow or white lines will be drawn to all of the other waypoints that the bot would consider to be "reachable". If the connection is a two-way connection the line is yellow, one-way connections appear white. These "reachable" waypoints would be waypoints that are clearly visible from the current location. Certain waypoints will be disallowed as reachable for one reason or another. For example, waypoints suspended in mid-air above the bot would not be considered reachable since the bot couldn't jump high enough to get to them. Also waypoints that are too far away from the current location would not be considered reachable. You may have waypoints that are close enough to each other, but across a wide gap that would be too wide to jump. If the far waypoint is close enough and clearly visible, it would still show as "reachable" since currently we have no method to determine if the bot can get to that waypoint or not.

The bots will ONLY go from one waypoint to another if there is a path between them. Get in the habit of turning on the pathwaypoint setting when creating waypoint files so that you can see the waypoint paths and will know whether or not a path exists between two waypoints. Also get in the habit of checking that paths exist BOTH WAYS between waypoints. Just because a path is drawn from point A to point B, doesn't mean that a path exists from point B to point A.

The ``yb path add`` command allows you to manually assign a path between 2 waypoints. This is needed in some cases where the waypoints are blocked (by doorways or other objects) and you wish to create a path between these waypoints. Move close to the waypoint you wish the path to start from and use the menu to add path.

The actual Waypoint Number you're standing on will be shown in the upper corner of your HUD (if you turned pathwaypoint on). For example to manually assign a path between Waypoint #250 and 251, you first should stand in the near of #250, then use ``yb path cache`` to cache waypont #250, then go to the waypoint #251 and type ``yb path create`` to show menu and create needed path connection (one-way or two-way). You can also do this by looking at needed waypoint instead of caching it.

The ``yb path delete`` command is just like the "add" command except that it removes a path (connection) from the starting point to the ending point. This is necessary in some cases where you may have a door that opens from one side and allows you to go through but once the door closes you can't go back through the other way.

.. figure:: static/img09.jpg
    :align: center

    Example of path connection.

.. figure:: static/img10.jpg
    :align: center

    Example of path connection.
    
When adding waypoints use the pathwaypoint feature to verify that waypoints are actually reachable. Sometimes you add a waypoint and it appears close enough and it also appears to be in plain sight, but the Half-Life engine doesn't indicate that the waypoint is "reachable". Be sure to check this in BOTH directions when defining a waypoint path. Usually you don't have to be too careful about waypoint placement, but in some situations, you may have to spend more time carefully laying out waypoints to get them to be "reachable" (ladders are a good example).

Alternatively (especially if you can't remember all these commands), you can also use the waypoint operating menu to edit waypoints. Press the "-" key and the menu will be shown. You can do all of waypoint operations there.

To set crouch waypoints, simply crouch when adding the waypoint. You will notice that the waypoint line is shorter than a normal waypoint. Bot will crouch automatically when approaching a crouch waypoint (if it's not a camping waypoint).

Try placing waypoints at intersections in hallways (do use some "important" waypoints at crossings). Try not to place waypoints in places where the bots would bump into corners of walls or other obstacles when trying to get from one waypoint to the next. Remember the bots won't always take what you consider to be the "obvious" choice when choosing the next waypoint. Try to limit the choices that they have to keep them from heading off in the wrong direction. Also remember that the Bots could go backward in a fight and might not find a waypoint. In that special case they wouldn't find a near enough waypoint (they only search in-between 200 Units) and get stuck forever. Also put some waypoints in corners or behind walls/doors so they can take good cover positions.

- The Bots will try to take cover at waypoints:

  + Not seen by the enemy and
  + The one with the smallest wayzone radius in their range 

What is the waypoint radis?
-------------------------
Displayed in yellow circles around the waypoints.

Bots will pick a point inside this radius for their destination origin (based on things like orientation etc.), this just makes movement not that "static". Ladder waypoints, camp waypoints and goal waypoints will always have a radius of 0. Don't try to change it! After bot calculated all Zones you should walk all over your waypoints to verify that they are reachable within their radius. Try to be careful with the radius. If your path bends around corners it's better to lower the radius. You should set the radius to 0 if the Path goes through some narrow doorways or similar. You manually assign a new value by standing near to the waypoint and typing ``yb wp setradius x``. Where x is the value you want to assign. Valid ranges are 0-128. The bigger, the wider it is. 

Some further advices
-------------------------

Start by typing ``yb wp on``. After that type ``yb autowp on``. Then try to cover the map with waypoints everywhere want the bots to roam. Then try to think of the important points for each team. Go to that position, delete the "normal" waypoint and instead set a strategical waypoint at that position. Then place the goal waypoints, rescue waypoints and ladder waypoints. After that calculate all wayzones and try to detect waypoints with radius mistakes. Then do a lot of test playing until finally satisfied with all the movement.
 