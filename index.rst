**************************
Yet Another POD-Bot Readme
**************************
**YaPB** is an AI opponent for Counter-Strike_ based on POD-Bot 2.6 which allows you to play good old Counter-Strike_ without connecting to any server, or fill your server with AI-controlled players.

.. Important::  YaPB 2.x series had some docs missing for almost a ten years. This is an attempt to fill the gap. As I'm too lazy, it's mostly based on another flavour's podbot_mm_ documentation. And the current status is deeply **WIP**.

If you have time and interest you can contribute to documentation on GitLab_ repository.

.. _Counter-Strike: https://store.steampowered.com/app/10/CounterStrike/
.. _podbot_mm: http://podbotmm.bots-united.com/
.. _GitLab: https://gitlab.com/jeefo/yapb-readme/

.. toctree::
   :caption: Bot Documentation
   :maxdepth: 2

   introduction
   installation
   configuring
   botusage
   waypointing
   credits